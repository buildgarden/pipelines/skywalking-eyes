# skywalking-eyes pipelines

<!-- BADGIE TIME -->

[![pipeline status](https://img.shields.io/gitlab/pipeline-status/buildgarden/pipelines/skywalking-eyes?branch=main)](https://gitlab.com/buildgarden/pipelines/skywalking-eyes/-/commits/main)
[![latest release](https://img.shields.io/gitlab/v/release/buildgarden/pipelines/skywalking-eyes)](https://gitlab.com/buildgarden/pipelines/skywalking-eyes/-/releases)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->

Check and fix license headers with [SkyWalking Eyes](https://github.com/apache/skywalking-eyes).

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

## Usage

### License-Eye configuration

Add a `.licenserc.yaml` file to your project. See the [skywalking-eyes
project](https://github.com/apache/skywalking-eyes#github-actions) for more
information.

Here's a short example:

```yaml
header:
  - comment: never
    license:
      spdx-id: MIT
      content: |-
        Copyright 2024 Example Org
        SPDX-License-Identifier: MIT
    paths-ignore:
      - "*.md"
      - "*.rst"
      - "*.json"
      - ".secrets.baseline"
      - LICENSE
      - NOTICE
```

### Check for license headers in GitLab CI

Add the following to your `.gitlab-ci.yml` file:

```yaml
include:
  - project: buildgarden/pipelines/skywalking-eyes
    file:
      - license-eye-header-check.yml
```

### Add missing licenses locally with pre-commit

Add the following to your `.pre-commit-config.yaml` file:

> NOTE: `license-eye` must be installed to use this hook.

```yaml
repos:
  - repo: https://gitlab.com/buildgarden/pipelines/skywalking-eyes
    rev: ""
    hooks:
      - id: license-eye-header-fix
```
